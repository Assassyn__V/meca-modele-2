Function fleche_perche(x As Double, resolution As Integer) As Double
    fleche_perche = Riemann(0, x, resolution, "fleche_seconde_perche")
End Function

Function erreur_fleche_perche(x As Double, resolution As Integer) As Double
    erreur_fleche_perche = ErreurRiemann(0, x, resolution, "fleche_seconde_perche")
End Function

Function Riemann(lower As Double, upper As Double, resolution As Integer, formula As String) As Double
    Call OptimizeCode_Begin
    
    'https://en.wikipedia.org/wiki/Trapezoidal_rule
    
    Dim ii As Integer
    
    Delta = (upper - lower) / resolution
    
    sum = prime(lower, lower, resolution, formula)
    
    For ii = 1 To resolution - 1
    
        sum = sum + 2 * prime(lower, lower + ii * Delta, resolution, formula)
    
    Next ii
    
    sum = sum + prime(lower, upper, resolution, formula)
    
    Call OptimizeCode_End
    
    Riemann = sum / 2 * Delta
End Function

Function prime(lower As Double, upper As Double, resolution As Integer, formula As String) As Double
    
    Call OptimizeCode_Begin
    
    Dim ii As Integer
    
    Delta = (upper - lower) / resolution
    
    sum = Application.Run(formula, lower)
    
    For ii = 1 To resolution - 1
    
        sum = sum + 2 * Application.Run(formula, lower + ii * Delta)
    
    Next ii
    
    sum = sum + Application.Run(formula, upper)
    
    Call OptimizeCode_End
    
    prime = sum / 2 * Delta
End Function