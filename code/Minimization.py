import sys
import numpy as np
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.integrate as integrate
import scipy.optimize as opt
from scipy.optimize import minimize

#---------- constants ----------#
GRAPH = 1 << 0
SOBOL = 1 << 1
CONSTANT = 1 << 2
DISABLE = 1 << 3
SAVE = 1 << 4
EPOXY = 1 << 5

# perche
L_p = 4 * math.sqrt(5)

# support
L_s = 1.5


# material constants

epoxy_price = 2700 #euro / T
epoxy_height = 0.001 #m
epoxy_vol = 1100 #kg/m3

# INOX 316L
E = 193000000000
rho = 7960
Re_02 = 205000000
price = 2643

# rho = 1992.9

#Acier lambda
# E = 210000000000
# rho = 7800
# Re_02 = 205000000
# price = 605


# alu
# E = 67500000000
# rho = 2700
# Re_02 = 30000000
# price = 1640

# other constants
R_B = 0
theta = math.pi / 2 - math.atan(2)
g = -10
rho_fluide = 1.2
C_Re = 1.5
v_infty = 50
alpha_vent = 0.5 * rho_fluide * C_Re * v_infty ** 2
eau = g * math.pi * 0.015**2/4 *0 #négligé

# cdcf
k = 1.5
max_fleche = 0.12

#---------- functions ----------#

# commons


def a(D_1, D_0, L):
    return (D_1 - D_0) / L


def a_e(e_1, e_0, L):
    return (e_1 - e_0) / L


def alpha(a, a_e):
    return a_e * a - a_e ** 2


def beta(a, a_e, b, b_e):
    return b * a_e + b_e * a - 2 * a_e * b_e


def gamma(b, b_e):
    return b_e * b - b_e**2


def IGZ(x, D_ext, D_int):
    # if D_ext == D_int:
    #     D_int = D_ext + 0.01
    return math.pi / 64 * (D_ext ** 4 - D_int ** 4)


def poidsEau(a, b):
    math.pi() / 4 * 0.015**2 * (b-a) * 1000


def fleche_seconde(t, p, s, L, callback_tcoh):
    Mfz = callback_tcoh(t, p, s)[5]

    d = p

    if (callback_tcoh == T_coh_s):
        d = s

    D_1 = d[0]
    D_0 = d[1]
    e_1 = d[2]
    e_0 = d[3]

    D_ext = a(D_1, D_0, L) * t + D_0
    D_int = D_ext - 2 * (a_e(e_1, e_0, L) * t + e_0)

    igz = IGZ(t, D_ext, D_int)

    ret = Mfz / (igz * E)

    return ret

# calcul la derivee de la fleche


def fleche_prime(t, p, s, L, callback_tcoh):
    # epsabs & esprel are set to those values to fix the approximation of the integral
    return integrate.quad(lambda x: fleche_seconde(x, p, s, L, callback_tcoh), 0, t, epsabs=0, epsrel=1)

# calcul la fleche


def fleche(x, p, s, L, callback_tcoh):
    # epsabs & esprel are set to those values to fix the approximation of the integral
    return integrate.dblquad(lambda x, y: fleche_seconde(x, p, s, L, callback_tcoh), 0, x, lambda t: 0, lambda t: t, epsabs=0, epsrel=0.1)


def sigma_N(x, p, s, L, callback_tcoh):
    d = p

    if (callback_tcoh == T_coh_s):
        d = s

    D_1 = d[0]
    D_0 = d[1]
    e_1 = d[2]
    e_0 = d[3]

    N = callback_tcoh(x, p, s)[0]
    D_ext = a(D_1, D_0, L) * x + D_0
    D_int = D_ext - 2 * (a_e(e_1, e_0, L) * x + e_0)

    S = math.pi / 4 * (D_ext ** 2 - D_int ** 2)

    return N / S


def sigma_Mfz(x, p, s, L, callback_tcoh):
    d = p

    if (callback_tcoh == T_coh_s):
        d = s

    D_1 = d[0]
    D_0 = d[1]
    e_1 = d[2]
    e_0 = d[3]

    D_ext = a(D_1, D_0, L) * x + D_0
    D_int = D_ext - 2 * (a_e(e_1, e_0, L) * x + e_0)

    Mfz = callback_tcoh(x, p, s)[5]
    igz = IGZ(x, D_ext, D_int)

    return -Mfz / igz * D_ext/2


def sigma_abs(x, p, s, L, callback_tcoh):
    mfz = sigma_Mfz(x, p, s, L, callback_tcoh)
    N = sigma_N(x, p, s, L, callback_tcoh)

    return abs(mfz) + abs(N)

# recupere le maximum sur une plage d'abscisse en entree


def sigma_max(x, p, s, L, callback_tcoh):
    res = 0
    for ii, val in enumerate(x):
        y = sigma_abs(val, p, s, L, callback_tcoh)
        if y > res:
            res = y
    return res


def functionValues(x, callback_singleVar):
    res = np.zeros_like(x)
    for ii, val in enumerate(x):
        y = callback_singleVar(val)
        res[ii] = y
    return res


# Perche


def P_L(x, p):
    D_1 = p[0]
    D_0 = p[1]
    e_1 = p[2]
    e_0 = p[3]

    a_p = a(D_1, D_0, L_p)
    a_e_p = a_e(e_1, e_0, L_p)
    alpha_p = alpha(a_p, a_e_p)
    beta_p = beta(a_p, a_e_p, D_0, e_0)
    gamma_p = gamma(D_0, e_0)

    return g * rho * math.pi * (
        alpha_p / 3 * (L_p ** 3 - x**3)
        + beta_p / 2 * (L_p ** 2 - x ** 2)
        + gamma_p * (L_p - x)
    )


def P_x(x, p):
    return math.cos(theta) * P_L(x, p)


def P_y(x, p):
    return -math.sin(theta) * P_L(x, p)


def P_M(x, p):
    D_1 = p[0]
    D_0 = p[1]
    e_1 = p[2]
    e_0 = p[3]

    a_p = a(D_1, D_0, L_p)
    a_e_p = a_e(e_1, e_0, L_p)
    alpha_p = alpha(a_p, a_e_p)
    beta_p = beta(a_p, a_e_p, D_0, e_0)
    gamma_p = gamma(D_0, e_0)

    return -math.sin(theta) * g * rho * math.pi * (
        alpha_p / 4 * (L_p ** 4 - x**4)
        + beta_p / 3 * (L_p ** 3 - x ** 3)
        + gamma_p / 2 * (L_p ** 2 - x ** 2)
    )


def V_y(x, p):
    D_1 = p[0]
    D_0 = p[1]

    a_p = a(D_1, D_0, L_p)

    return alpha_vent * (
        a_p / 2 * (L_p ** 2 - x ** 2)
        + D_0 * (L_p - x)
    )


def V_M(x, p):
    D_1 = p[0]
    D_0 = p[1]

    a_p = a(D_1, D_0, L_p)

    return alpha_vent * (
        a_p / 3 * (L_p ** 3 - x ** 3)
        + D_0 / 2 * (L_p**2 - x**2)
    )


def E_p(x):
    return eau * L_p * (L_p - x)


def M_p(x):
    return eau * L_p * math.cos(theta) * (L_p - x)**2 / 2

# s is shallow there


def T_coh_p(x, p, s):
    return (
        # column #1
        R_B * math.sin(theta) + P_x(x, p) - E_p(x) * math.sin(theta),
        R_B * math.cos(theta) + P_y(x, p) + V_y(x, p) + \
        E_p(x) * math.cos(theta),
        0,
        # column #2
        0,
        0,
        (L_p - x) * R_B * math.cos(theta) + P_M(x, p) - x * \
        P_y(x, p) + V_M(x, p) - x * V_y(x, p) + M_p(x)
    )

# Support


def H_p(p):
    return R_B * math.cos(theta) + P_y(0, p) + V_y(0, p)


def N_p(p):
    return L_p * R_B * math.cos(theta) + P_M(0, p) + V_M(0, p)


def Px_s(x, p):
    D_1 = p[0]
    D_0 = p[1]
    e_1 = p[2]
    e_0 = p[3]

    a_s = a(D_1, D_0, L_s)
    a_e_s = a_e(e_1, e_0, L_s)
    alpha_s = alpha(a_s, a_e_s)
    beta_s = beta(a_s, a_e_s, D_0, e_0)
    gamma_s = gamma(D_0, e_0)

    return g * rho * math.pi * (
        alpha_s / 3 * (L_s ** 3 - x ** 3)
        + beta_s / 2 * (L_s ** 2 - x ** 2)
        + gamma_s * (L_s - x)
    )


def V_y_s(x, p):
    D_1 = p[0]
    D_0 = p[1]

    a_s = a(D_1, D_0, L_s)

    return alpha_vent * (
        a_s / 2 * (L_s ** 2 - x**2)
        + D_0 * (L_s - x)
    )


def V_M_s(x, p):
    D_1 = p[0]
    D_0 = p[1]

    a_s = a(D_1, D_0, L_s)

    return alpha_vent * (
        a_s / 3 * (L_s ** 3 - x ** 3)
        + D_0 / 2 * (L_s ** 2 - x ** 2)
    )


def E_s(x):
    return eau * L_s * (L_s - x)


def T_coh_s(x, p, s):
    return (
        P_x(0, p) * math.cos(theta) - H_p(p) *
        math.sin(theta) + Px_s(x, s) + E_s(x),
        P_x(0, p) * math.sin(theta) + H_p(p) * math.cos(theta) + V_y_s(x, s),
        0,
        0,
        0,
        (L_s - x) * (
            P_x(x, p) * math.sin(theta) +
            H_p(p) * math.cos(theta)
        )
        + N_p(p) +
        V_M_s(x, s) - x * V_y_s(x, s)
    )

# together


def fleche_total_index(index, fleche_p, fleche_s, fleche_prime_s, sigma_support, ecrasement_support, ecrasement_perche):
    perche_x = ecrasement_perche[index]
    perche_y = fleche_p[index]

    support_x = ecrasement_support[index] \
        - sigma_support * L_p * math.cos(theta) \
        - L_p * fleche_prime_s[index] * math.sin(theta)

    support_y = fleche_s[index] \
        + sigma_support * L_p * math.sin(theta) \
        + L_p * fleche_prime_s[index] * math.cos(theta)

    fleche_x = perche_x * math.cos(theta) - \
        perche_y * math.sin(theta) + support_x
    fleche_y = perche_x * math.sin(theta) + \
        perche_y * math.cos(theta) + support_y

    return math.sqrt(fleche_x ** 2 + fleche_y ** 2)


def fleche_totale(p, s):
    fleche_p = fleche(L_p, p, s, L_p, T_coh_p)
    fleche_s = fleche(L_s, p, s, L_s, T_coh_s)
    fleche_prime_s = fleche_prime(L_s, p, s, L_s, T_coh_s)
    sigma_support = sigma_N(L_s, p, s, L_s, T_coh_s) / E
    ecrasement_support = ecrasement(L_s, p, s, L_s, T_coh_s)
    ecrasement_perche = ecrasement(L_p, p, s, L_p, T_coh_p)

    return (
        fleche_total_index(0, fleche_p, fleche_s, fleche_prime_s,
                           sigma_support, ecrasement_support, ecrasement_perche),
        fleche_total_index(1, fleche_p, fleche_s, fleche_prime_s,
                           sigma_support, ecrasement_support, ecrasement_perche)
    )


def mass(p, s):
    D_1 = s[0]
    D_0 = s[1]
    e_1 = s[2]
    e_0 = s[3]

    a_s = a(D_1, D_0, L_s)
    a_e_s = a_e(e_1, e_0, L_s)
    alpha_s = alpha(a_s, a_e_s)
    beta_s = beta(a_s, a_e_s, D_0, e_0)
    gamma_s = gamma(D_0, e_0)

    D_1 = p[0]
    D_0 = p[1]
    e_1 = p[2]
    e_0 = p[3]

    a_p = a(D_1, D_0, L_p)
    a_e_p = a_e(e_1, e_0, L_p)
    alpha_p = alpha(a_p, a_e_p)
    beta_p = beta(a_p, a_e_p, D_0, e_0)
    gamma_p = gamma(D_0, e_0)

    return rho * math.pi * (
        alpha_p / 3 * L_p ** 3 + beta_p / 2 * L_p ** 2 + gamma_p * L_p
        + alpha_s / 3 * L_s ** 3 + beta_s / 2 * L_s ** 2 + gamma_s * L_s
    )

    #------ TESTS


def ecrasement(max, p, s, L, callback_tcoh):
    return integrate.quad(lambda x: sigma_N(x, p, s, L, callback_tcoh) / E, 0, max)


def area(d, L):
    D_1 = d[0]
    D_0 = d[1]

    a_s = a(D_1, D_0, L)

    return math.pi / 2 * (a_s/2 * L**2 + D_0 * L)


def epoxy(p, s):
    area_p = area(p, L_p)
    volume_p = area_p * epoxy_height
    area_s = area(s, L_s)
    volume_s = area_s * epoxy_height

    return (volume_p, volume_s)


    #--------------utils------------------#
formatter = mpl.ticker.EngFormatter()


def print_Torseur(arr):
    for ii in range(0, 3):
        print("| {0: 10.3f}    {0: 10.3f} |".format(arr[ii], arr[ii + 3]))


def sigmaConstraint(x, L, limit, callback_tcoh):
    p = (
        x[0],
        x[1],
        x[2],
        x[3],
    )

    s = (
        x[4],
        x[5],
        x[6],
        x[7],
    )

    # all the points to approximate where the max is
    t = np.linspace(0, L)

    return sigma_max(t, p, s, L, callback_tcoh) - limit


def flecheConstraint(x):
    p = (
        x[0],
        x[1],
        x[2],
        x[3]
    )

    s = (
        x[4],
        x[5],
        x[6],
        x[7]
    )

    fleche_calculee = fleche_totale(p, s)

    return abs(fleche_calculee[0]) + abs(fleche_calculee[1]) - max_fleche


def objective_function(x):
    p = (
        x[0],
        x[1],
        x[2],
        x[3]
    )

    s = (
        x[4],
        x[5],
        x[6],
        x[7]
    )

    res = mass(p, s)

    print("mass:", '{:05.3f}'.format(res))

    # return res ** 2 / 10000

    return res ** 2 / 10000


def minimizeCost():
    bnds = [
        (0.025, 0.9),
        (0.025, 0.9),
        (0.005, 0.05),
        (0.005, 0.05),
        (0.025, 0.9),
        (0.025, 0.9),
        (0.005, 0.05),
        (0.005, 0.05)
    ]

    cons = [
        {'type': 'ineq', 'fun': lambda x: -
            sigmaConstraint(x, L_s, Re_02 / k, T_coh_s)},
        {'type': 'ineq', 'fun': lambda x: -
            sigmaConstraint(x, L_p, Re_02 / k, T_coh_p)},
        {'type': 'ineq', 'fun': lambda x: -flecheConstraint(x)},
        #2 * epaisseur <= diametre
        {'type': 'ineq', 'fun': lambda x: -2*x[2] + x[0]},
        {'type': 'ineq', 'fun': lambda x: -2*x[3] + x[1]},
        {'type': 'ineq', 'fun': lambda x: -2*x[6] + x[4]},
        {'type': 'ineq', 'fun': lambda x: -2*x[7] + x[5]},
    ]

    if commands & CONSTANT:
        cons.append({'type': 'eq', 'fun': lambda x: x[0] - x[1]})
        cons.append({'type': 'eq', 'fun': lambda x: x[2] - x[3]})
        cons.append({'type': 'eq', 'fun': lambda x: x[4] - x[5]})
        cons.append({'type': 'eq', 'fun': lambda x: x[6] - x[7]})

    # routine de prise en compte des arguments passés au script
    if commands & SOBOL:
        ret = opt.shgo(objective_function, bounds=bnds, constraints=cons,
                       iters=10, n=50, sampling_method='sobol', options={'disp': True})
    else:
        ret = opt.shgo(objective_function, bounds=bnds,
                       constraints=cons, iters=10, n=50, options={'disp': True})

    return ret


def printVent(p, s):
    D_1 = s[0]
    D_0 = s[1]

    a_s = a(D_1, D_0, L_s)

    print('Support origine: ', formatter(alpha_vent * (D_0)),
          'N/m extremite: ', formatter(alpha_vent * (a_s * L_s + D_0)), 'N/m')

    D_1 = p[0]
    D_0 = p[1]

    a_p = a(D_1, D_0, L_p)
    print('Perche origine: ', formatter(alpha_vent * (D_0)),
          'N/m extremite: ', formatter(alpha_vent * (a_p * L_p + D_0)), 'N/m')


#--------------execution--------------#
def printResults(p_0, s_0):
    # Perche
    print("Choix des valeurs:")
    print("Perche :")
    print("                extrémité   origine ")
    print("diamètre  : {:>10}m   {:>10}m".format(
        formatter(p_0[0]), formatter(p_0[1])))
    print("épaisseur : {:>10}m   {:>10}m".format(
        formatter(p_0[2]), formatter(p_0[3])))

    print("\nSupport :")
    print("                extrémité   origine ")
    print("diamètre  : {:>10}m   {:>10}m".format(
        formatter(s_0[0]), formatter(s_0[1])))
    print("épaisseur : {:>10}m   {:>10}m".format(
        formatter(s_0[2]), formatter(s_0[3])))

    print('\n--------------------------------\n')

    # print('Charge de l\'eau:', formatter(eau), 'N/m')
    print('Vent:')
    printVent(p_0, s_0)
    print('\n--------------------------------\n')

    print('Calcul du torseur de cohésion de la perche:')
    coh_p = T_coh_p(0, p_0, s_0)
    print_Torseur(coh_p)
    fleche_p = fleche(L_p, p_0, s_0, L_p, T_coh_p)
    print("Calcul de la flèche: ", formatter(
        fleche_p[0]), "m +- ", formatter(fleche_p[1]), "m")

    print('\n--------------------------------\n')
    t_p = np.linspace(0, L_p)

    sigma = sigma_max(t_p, p_0, s_0, L_p, T_coh_p)
    print('Contrainte maximale :', formatter(sigma), 'Pa')

    delta = ecrasement(L_p, p_0, s_0, L_p, T_coh_p)
    print('Deplacement dû à la contrainte normale',
          formatter(delta[0]), '+-', formatter(delta[1]), 'm')

    print('\n--------------------------------\n')

    print('Calcul du torseur de cohésion du support:')
    coh_s = T_coh_s(L_s, p_0, s_0)
    print_Torseur(coh_s)
    fleche_s = fleche(L_s, p_0, s_0, L_s, T_coh_s)
    print("\nCalcul de la flèche: ", formatter(
        fleche_s[0]), 'm +-', formatter(fleche_s[1]), 'm')

    t_s = np.linspace(0, L_s)

    sigma = sigma_max(t_s, p_0, s_0, L_s, T_coh_s)
    print('Contrainte maximale: ', formatter(sigma), 'Pa')

    delta = ecrasement(L_s, p_0, s_0, L_s, T_coh_s)
    print('Deplacement dû à la contrainte normale :',
          formatter(delta[0]), '+-', formatter(delta[1]), 'm')

    print('\n--------------------------------\n')

    fleche_tot = fleche_totale(p_0, s_0)
    print("Flèche:", formatter(fleche_tot[0]),
          'm +-', formatter(fleche_tot[1]), 'm')

    print('\n--------------------------------\n')

    m = mass(p_0, s_0)
    print('masse :', formatter(m), "kg")
    print('prix :', formatter(m / 1000 * price), '€')
    
    if (commands & EPOXY):
        print('\n--------------------------------\n')
        epoxy_calc = epoxy(p_0, s_0)
        mass_epoxy = (epoxy_calc[0] + epoxy_calc[1]) * epoxy_vol / 1000
        print('Ajout d\'epoxy : ', formatter(mass_epoxy), 'kg (sur', formatter(epoxy_height), 'm d\'épaisseur')
        price_epoxy = mass_epoxy * epoxy_price
        print('Pour un prix de : ', formatter(price_epoxy), '€')
        
        print('\n--------------------------------\n')

        print('Total price: ', formatter(price_epoxy + m / 1000 * price), '€')





def graphResults():
    print('Starting to graph...')

    t_p = np.linspace(0, L_p)
    t_s = np.linspace(0, L_s)
    
    plt.figure(1)

    if not commands & SAVE:
        plt.subplot(221)

    plt.plot(t_p, functionValues(t_p, lambda x: fleche(
        x, p_0, s_0, L_p, T_coh_p)[0]), 'b-')
    plt.title('Perche : fleche')
    plt.xlabel('Position x (m)')
    plt.ylabel('Flèche (m)')

    if not commands & SAVE:
        plt.subplot(222)
    else:
        plt.savefig('fig_1.png', transparent=True)
        plt.figure(2)

    plt.plot(t_p, functionValues(t_p, lambda x: sigma_abs(
        x, p_0, s_0, L_p, T_coh_p)), 'r-')
    plt.title('Perche : contrainte normale + Mfz')
    plt.xlabel('Position x (m)')
    plt.ylabel('Contrainte (Pa)')

    if not commands & SAVE:
        plt.subplot(223)
    else:
        plt.savefig('fig_2.png', transparent=True)
        plt.figure(3)

    plt.plot(t_s, functionValues(t_s, lambda x: fleche(
        x, p_0, s_0, L_s, T_coh_s)[0]), 'b-')
    plt.title('Support : fleche')
    plt.xlabel('Position x (m)')
    plt.ylabel('Flèche (m)')
    
    if not commands & SAVE:
        plt.subplot(224)
    else:
        plt.savefig('fig_3.png', transparent=True)
        plt.figure(4)

    plt.plot(t_s, functionValues(t_s, lambda x: sigma_abs(
        x, p_0, s_0, L_s, T_coh_s)), 'r-')
    plt.title('Support : contrainte normale + Mfz')
    plt.xlabel('Position x (m)')
    plt.ylabel('Contrainte (Pa)')

    if (commands & SAVE):
        plt.savefig('fig_4.png', transparent=True)
    else:
        plt.show()


def analyzeScriptArgs():
    ret = 0
    for arg in sys.argv:
        if arg == '--graph' or arg == '-g':
            ret = ret | GRAPH
        elif arg == '--sobol' or arg == '-s':
            ret = ret | SOBOL
        elif arg == '--constant-section' or arg == '-c':
            ret = ret | CONSTANT
        elif arg == '--disable' or arg == '-d':
            ret = ret | DISABLE
        elif arg == '--save':
            ret = ret | SAVE
        elif arg == '--epoxy':
            ret = ret | EPOXY

    return ret

# try to minimize


commands = analyzeScriptArgs()

if not (commands & DISABLE):
    res = minimizeCost()

    print('Optimization results: ')
    print(res)
    print('----------------')

    x = res['x']
else:
    # x = [0.02898933, 0.27850003, 0.005, 0.005, 0.28033567,
    #      0.31716061, 0.005, 0.005]
    # x = [0.84226149, 0.84226149, 0.00802485, 0.00802485, 0.9,
    #      0.9, 0.00924077, 0.00924077]
    # x= [0.5, 0.5, 0.01, 0.01, 0.5, 0.5, 0.01, 0.01]
    x = [0.025     , 0.21822448, 0.005     , 0.005     , 0.24480375,
       0.27624855, 0.005     , 0.005     ]


# avec épaisseur de 0.005 m
# x = [0.02898937, 0.27849979, 0.005     , 0.005     , 0.28033648,
#        0.31716096, 0.005     , 0.005     ]


p_0 = (
    x[0],
    x[1],
    x[2],
    x[3]
)

s_0 = (
    x[4],
    x[5],
    x[6],
    x[7]
)


printResults(p_0, s_0)


#---- plot perche ----#
if commands & GRAPH:
    graphResults()
