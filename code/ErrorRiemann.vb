Function ErreurRiemann(lower As Double, upper As Double, resolution As Integer, formula As String) As Double

    Dim max_seconde As Double, max_prime As Double, ii As Integer
    Delta = (upper - lower) / resolution
    max_seconde = 0
    
    For ii = 0 To resolution
        tmp = Application.Run(formula, lower + Delta * ii)
        
        If (tmp > max_seconde) Then
            max_seconde = tmp
        End If
    Next ii
    
    For ii = 0 To resolution
        tmp = prime(lower, lower + Delta * ii, resolution, formula)
        
        If (tmp > max_prime) Then
            max_prime = tmp
        End If
    Next ii
    
    ErreurRiemann = -1 * Delta ^ 3 / (12 * resolution ^ 2) * (max_seconde + max_prime)
    
End Function