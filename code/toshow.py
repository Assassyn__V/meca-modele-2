#calcul la derivee de la fleche
def fleche_prime(t, p, s, L, callback_tcoh):
    #epsabs & esprel are set to those values to fix the approximation of the integral
    return integrate.quad(lambda x: fleche_seconde(x, p, s, L, callback_tcoh), 0, t, epsabs=0, epsrel=1)

# calcul la fleche
def fleche(x, p, s, L, callback_tcoh):
    #epsabs & esprel are set to those values to fix the approximation of the integral
    return integrate.dblquad(lambda x, y: fleche_seconde(x, p, s, L, callback_tcoh), 0, x, lambda t: 0, lambda t: t, epsabs=0, epsrel=0.1)

#morcelement pour aider a la lisibilite du code de calcul de la fleche totale, ce sont les caluls en soi...
def fleche_total_index(index, fleche_p, fleche_s, fleche_prime_s, sigma_support, ecrasement_support, ecrasement_perche):
    perche_x = ecrasement_perche[index]
    perche_y = fleche_p[index]

    support_x = ecrasement_support[index] \
        - sigma_support * L_p * math.cos(theta) \
        - L_p * fleche_prime_s[index] * math.sin(theta)

    support_y = fleche_s[index] \
        + sigma_support * L_p * math.sin(theta) \
        + L_p * fleche_prime_s[index] * math.cos(theta)

    fleche_x = perche_x * math.cos(theta) - \
        perche_y * math.sin(theta) + support_x
    fleche_y = perche_x * math.sin(theta) + \
        perche_y * math.cos(theta) + support_y

    return math.sqrt(fleche_x ** 2 + fleche_y ** 2)

#calul la fleche totale en tenant compte de la fleche due a la flexion et a la compression
def fleche_totale(p, s):
    fleche_p = fleche(L_p, p, s, L_p, T_coh_p)
    fleche_s = fleche(L_s, p, s, L_s, T_coh_s)
    fleche_prime_s = fleche_prime(L_s, p, s, L_s, T_coh_s)
    sigma_support = sigma_N(L_s, p, s, L_s, T_coh_s) / E
    ecrasement_support = ecrasement(L_s, p, s, L_s, T_coh_s)
    ecrasement_perche = ecrasement(L_p, p, s, L_p, T_coh_p)

    return (
        fleche_total_index(0, fleche_p, fleche_s, fleche_prime_s,
                           sigma_support, ecrasement_support, ecrasement_perche),
        fleche_total_index(1, fleche_p, fleche_s, fleche_prime_s,
                           sigma_support, ecrasement_support, ecrasement_perche)
    )

#recupere le maximum sur une plage d'abscisse en entree
def sigma_max(x, p, s, L, callback_tcoh):
    res = 0
    for ii, val in enumerate(x):
        y = sigma_abs(val, p, s, L, callback_tcoh)
        if y > res:
            res = y
    return res


def minimizeCost():
    bnds = [
        (0.025, 0.9),
        (0.025, 0.9),
        (0.005, 0.05),
        (0.005, 0.05),
        (0.025, 0.9),
        (0.025, 0.9),
        (0.005, 0.05),
        (0.005, 0.05)
    ]

    cons = (
        {'type': 'ineq', 'fun': lambda x: -
            sigmaConstraint(x, L_s, Re_02 / k, T_coh_s)},
        {'type': 'ineq', 'fun': lambda x: -
            sigmaConstraint(x, L_p, Re_02 / k, T_coh_p)},
        {'type': 'ineq', 'fun': lambda x: -flecheConstraint(x)},
        #2 * epaisseur <= diametre
        {'type': 'ineq', 'fun': lambda x: -2*x[2] + x[0]},
        {'type': 'ineq', 'fun': lambda x: -2*x[3] + x[1]},
        {'type': 'ineq', 'fun': lambda x: -2*x[6] + x[4]},
        {'type': 'ineq', 'fun': lambda x: -2*x[7] + x[5]},
    )

    # routine de prise en compte des arguments passes au script
    if commands & SOBOL:
        ret = opt.shgo(objective_function, bounds=bnds, constraints=cons,
                       iters=10, n=50, sampling_method='sobol', options={'disp': True})
    else:
        ret = opt.shgo(objective_function, bounds=bnds,
                       constraints=cons, iters=10, n=50, options={'disp': True})

    return ret